import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import {User} from "./schemas/user.schema";
import {CreateUserDto} from "./dto/create-user.dto";

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get("/user")
  getUser(): Promise<User[]>{
    return this.appService.findAll();
  }

  @Post("/user")
  createUser(@Body() createUserDto:CreateUserDto){
    return this.appService.create(createUserDto);
  }
}
