import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { Model } from 'mongoose';
import { User, UserDocument } from "./schemas/user.schema";
import {CreateUserDto} from "./dto/create-user.dto";

@Injectable()
export class AppService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>
  ) { }

  getHello(): string {
    return 'Hello World!';
  }

  create(createUserDto: CreateUserDto): Promise<User>{
    const createdUser = this.userModel.create(createUserDto);
    return createdUser;
  }

  findAll(): Promise<User[]>{
    return this.userModel.find().exec();
  }
}
