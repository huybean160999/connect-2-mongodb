import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import {Document} from "mongoose";

export type UserDocument = User & Document;

@Schema()
export class User {
    @Prop({
        type: String
    })
    name: string;
  
    @Prop({
        type: Number
    })
    phone: number;
  
    @Prop({
        type: String
    })
    password: string;
  }
  
  export const UserSchema = SchemaFactory.createForClass(User);